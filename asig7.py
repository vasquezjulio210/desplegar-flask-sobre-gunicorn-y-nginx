from flask import Flask,render_template,request,redirect,url_for,flash

from flask_mysqldb import MySQL

app = Flask(__name__)


app.config['MYSQL_HOST']='127.0.0.1'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='password'
app.config['MYSQL_DB']='wordsflask'
mysql = MySQL(app)


app.secret_key = 'mysecretkey'


@app.route('/')
def Index():
    curl = mysql.connection.cursor()
    curl.execute('SELECT * FROM asig7')
    data = curl.fetchall()
    print(data)
    return render_template('index.html',asig7=data)

@app.route('/add_word',methods=['POST'])
def add_word():
    if request.method == 'POST':
        PALABRA = request.form['PALABRA']
        SIGNIFICADO1 = request.form['SIGNIFICADO1']
    
        
        curl = mysql.connection.cursor()
        curl.execute('INSERT INTO diccionario4 (PALABRA,SIGNIFICADO1) VALUES (%s,%s)',(PALABRA,SIGNIFICADO1))
        mysql.connection.commit()
        
    flash('Palabra Agregada exitosamente!')    
    return redirect(url_for('Index'))

@app.route('/edit/<ID>')
def edit_word(ID):
    curl = mysql.connection.cursor()
    curl.execute('SELECT * FROM diccionario4 WHERE ID = %s',(ID))
    data = curl.fetchall()

    return render_template('edit.html',PALABRA = data[0])

@app.route('/update/<id>', methods = ['POST'])
def update_word(id):
    if (request.method =='POST'):
        PALABRA = request.form['PALABRA']
        SIGNIFICADO1 = request.form['SIGNIFICADO1']
        curl = mysql.connection.cursor()
        curl.execute('UPDATE diccionario4 SET PALABRA = %s, SIGNIFICADO = %s WHERE ID = %s ',(PALABRA,SIGNIFICADO1,ID))
        mysql.connection.commit()
        flash('La Palabra ha sido actualizada')
    return redirect(url_for('Index'))

@app.route('/delete/<string:ID>')
def delete_word(ID):
    curl = mysql.connection.cursor()
    curl.execute('DELETE FROM asig7 WHERE ID = {0}'.format(ID))
    mysql.connection.commit()
    flash('La Palabra ha sido borrada exitosamente!!')
    return redirect(url_for('Index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0')
